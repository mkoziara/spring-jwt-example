package pl.pacific112.jwt.users;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class UsersController {

    private List<User> inMemoryUsers = new ArrayList<>(Arrays.asList(
            new User("pacific112", "testPassword"),
            new User("analebed", "wilkojad")
    ));

    @GetMapping("/users")
    public List<User> getUsers() {
        return inMemoryUsers;
    }

    @PostMapping("/seller/users")
    public void addUser(@RequestBody User user) {
        inMemoryUsers.add(user);
    }
}