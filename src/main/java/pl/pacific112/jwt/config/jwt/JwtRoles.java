package pl.pacific112.jwt.config.jwt;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

class JwtRoles {

    private final List<String> authority;

    @JsonCreator
    public JwtRoles(@JsonProperty("authority") List<String> authority) {
        this.authority = authority;
    }

    public List<String> getAuthority() {
        return authority;
    }

    @JsonIgnore
    public List<? extends GrantedAuthority> getGrantedAuthorities() {
        return authority.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}