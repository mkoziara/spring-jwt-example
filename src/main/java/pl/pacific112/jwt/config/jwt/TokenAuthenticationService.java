package pl.pacific112.jwt.config.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TokenAuthenticationService {

    private static final long EXPIRATION_TIME = 3600 * 1000;
    private static final String TOKEN_PREFIX = "Bearer ";

    @Value("${jwt.secret}")
    private String secret;

    String createAuthenticationHeader(Authentication authResult) {
        List<String> authorities = authResult.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        return Jwts.builder()
                .setSubject(authResult.getName())
                .claim("roles", new JwtRoles(authorities))
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (token != null) {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody();
            String user = body.getSubject();
            JwtRoles roles = getRoles(body.get("roles"));
            return (user != null) ? new UsernamePasswordAuthenticationToken(user, null, roles.getGrantedAuthorities()) : null;
        }

        return null;
    }

    // jwtk mimo ze ma serializowanie do jsona to brakuje mu deserializacji (wszystko jest LinkedHashMap)
    // przez co trzeba to zrobic recznie
    private static JwtRoles getRoles(Object roles) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String s = objectMapper.writeValueAsString(roles);
            return objectMapper.readValue(s, JwtRoles.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}